<?php
	require_once 'sql.php';
  $res = array();
	$sql="SELECT min(valor_minimo) minimo,max(valor_maximo) maximo FROM freyjapp.moteles
		where valor_minimo>0;";
    $mysqli=crearConexion();
  if($resultado=$mysqli->query($sql)){
    $res["valores"] = $resultado->fetch_array(MYSQLI_ASSOC);
    $res["success"] = true;
  }else {
    $res["success"] = false;
  }
  $resultado->free();

	$sql2="SELECT codigo,descripcion FROM freyjapp.ciudades
		where codigo>0
		order by descripcion;";

  $res["ciudades"] = array();
  if($resultado=$mysqli->query($sql2)){
    while($row = $resultado->fetch_array(MYSQLI_ASSOC)){
      array_push($res['ciudades'], $row);
    }
  }else{
    $res["success"] = false;
  }
  $resultado->free();
  $mysqli->close();

	header("Content-type: application/json");
	echo json_encode($res);
?>
