<?php
// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'AIzaSyB-S-ldLYCHht3Zztw89zSszxdrfArdkWc' );
// prep the bundle
$msg = array
(
	'aceptacion' => 'true',
	'cancelacion' => 'false'
);

$fields = array
(
	'to' 	=> $_POST['token'],
	'data'	=> $msg
);

$headers = array
(
	'Authorization: key=' . API_ACCESS_KEY,
	'Content-Type: application/json'
);

$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );
echo $result;
?>
