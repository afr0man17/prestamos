<?php
// API access key from Google API's Console
define( 'API_ACCESS_KEY', 'AIzaSyB-S-ldLYCHht3Zztw89zSszxdrfArdkWc' );
$registrationIds = array( $_POST['token'] );
// prep the bundle
$msg = array
(
	'aceptacion' => 'false',
	'cancelacion' => 'false'
);

$notification = array
(
	'body' => 'Tu reserva vence en 10 minutos.',
	'title'		=> $_POST['motel'],
	'badge' => '1',
	'sound' => 'notification',
	'icon' => 'ic_stat_ic_notification'
);

$fields = array
(
	'to' 	=> $_POST['token'],
	'data'			=> $msg,
	'priority' => 'high',
	'notification' => $notification
);

$headers = array
(
	'Authorization: key=' . API_ACCESS_KEY,
	'Content-Type: application/json'
);

$ch = curl_init();
curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
curl_setopt( $ch,CURLOPT_POST, true );
curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
$result = curl_exec($ch );
curl_close( $ch );
echo $result;
?>
