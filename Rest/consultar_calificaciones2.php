<?php
	require_once 'sql.php';
  $codigo=$_POST["codigo"];
	$sql="call consultar_calificaciones($codigo);";
	//$res=json_decode(sql2json($sql),true);

  $mysqli=crearConexion();

  $res["calificaciones"] = array();
  if($resultado = $mysqli->query($sql)){
    $res["success"] = true;
    while($row = $resultado->fetch_array(MYSQLI_ASSOC)){
      array_push($res['calificaciones'], $row);
    }
  }else{
    $res["success"] = false;
    $res['error'] = $mysqli->error;
  }
  $resultado->free();
  $mysqli->close();
  $mysqli=crearConexion();

	$sql2="call consultar_comentarios($codigo);";
  $res["comentarios"] = array();
  if($resultado = $mysqli->query($sql2)){
    $res["success"] = true;
    while($row = $resultado->fetch_array(MYSQLI_ASSOC)){
      array_push($res['comentarios'], $row);
    }
  }else{
    $res["success"] = false;
    $res['error'] = $mysqli->error;
  }


  header("Content-type: application/json");
  echo json_encode($res);
?>
