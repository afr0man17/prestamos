<?php
  include_once 'sql.php';
  $ciudad="";
  $max="";
  $min="";
  $rating="";
  $json= json_decode(file_get_contents('php://input'),true);
  if($json!=null){
    $ciudad=$json["ciudad"];
    $max=$json["max"];
    $min=$json["min"];
    $rating=$json["rating"];
  }else{
    $ciudad=$_POST["ciudad"];
    $max=$_POST["max"];
    $min=$_POST["min"];
    $rating=$_POST["rating"];
  }

  if($ciudad!="0"){
    $ciudad=" and m.ciudad=".$ciudad;
  }else{
    $ciudad="";
  }
  if($max!="0"){
    $max=" and m.valor_minimo<=".$max;
  }else{
    $max="";
  }
  if($min!="0"){
    $min=" and m.valor_minimo>=".$min;
  }else{
    $min="";
  }
  if($rating!="0"){
    $rating=" HAVING rating>=".$rating;
  }else{
    $rating="";
  }


  $sql="SELECT m.codigo,m.nombre,m.direccion,m.telefono,m.valor_minimo,m.valor_maximo,
			concat(c.descripcion,', ',d.descripcion) as ciudad,rating(m.codigo) as rating,m.horas_ocasional as horas
		from moteles m
		inner join ciudades c
			on c.codigo=m.ciudad
		inner join departamentos d
			on d.codigo=m.departamento
		where estado=1 and sector=0
			$ciudad $max $min $rating
		union
		select m.codigo,m.nombre,m.direccion,m.telefono,m.valor_minimo,m.valor_maximo,
			concat(s.descripcion,', ',c.descripcion,', ',d.descripcion) as ciudad,rating(m.codigo) as rating,m.horas_ocasional as horas
		from moteles m
			inner join ciudades c
		on c.codigo=m.ciudad
			inner join sectores s
		on s.codigo=m.sector
			inner join departamentos d
			on d.codigo=m.departamento
		where estado=1 and sector<>0
			$ciudad $max $min $rating
		order by rating desc;";

    $mysqli=crearConexion();

    $res["moteles"] = array();
    if($resultado = $mysqli->query($sql)){
      $res["success"] = true;
      while($row = $resultado->fetch_array(MYSQLI_ASSOC)){
        array_push($res['moteles'], $row);
      }
    }else{
      $res["success"] = false;
      $res['error'] = $mysqli->error;
    }
    $resultado->free();
    $mysqli->close();
    header("Content-type: application/json");
    echo json_encode($res);
?>
