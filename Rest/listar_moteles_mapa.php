<?php
	require_once 'sql.php';

	$resultado = array();
	$sql="call listar_moteles_mapa();";

	//$resultado["sql"]=$sql;
	$result=ConsultarSql($sql);
	$num_rows=mysql_num_rows($result);
	if(!$result){
    	$resultado["success"]=false;
  }
	else if ($num_rows>0){
		$resultado["success"]=true;
		$resultado["total"]=$num_rows;
		$resultado["moteles"]=array();
    while($row = mysql_fetch_array($result)) {
			//habitaciones disponibles
			$disponibles=false;
			if($row['disponibles']>0)
				$disponibles=true;
			//permite disponibilidad
			$disponibilidad=false;
			if($row["disponibilidad"]==1)
				$disponibilidad=true;
      $moteles = array(
          'codigo' => $row['codigo'],
          'nombre' => $row['nombre'],
          'direccion'=>$row['direccion'],
          'latitud'=>$row['latitud'],
          'longitud'=>$row['longitud'],
					'comodidades'=>$row['comodidades'],
					'rating'=>$row['rating'],
					'disponibilidad'=>$disponibilidad,
					'disponibles'=>$disponibles);
      array_push($resultado["moteles"], $moteles);
    }
	}else{
		$resultado["success"]=false;
	}
	header("Content-type: application/json");
	echo json_encode($resultado);
?>
