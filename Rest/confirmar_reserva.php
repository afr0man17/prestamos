<?php
  include_once 'sql.php';

  $codReserva = $_POST['reserva'];

  $sql2 = "call confirmar_reserva($codReserva)";
  $mysqli=crearConexion();
  if($resultado=$mysqli->query($sql2)){
    $row = $resultado->fetch_array(MYSQLI_ASSOC);
    if($row['token']!=''){
      $params=array(
        'token'=>$row['token'],
        'reserva'=>$codReserva,
      );
      $defaults = array(
        CURLOPT_URL => 'https://freyjapp-rojasdev.rhcloud.com/Rest/notificar_confirmacion.php',
        CURLOPT_POST => true,
        CURLOPT_POSTFIELDS => $params,
      );
      $ch = curl_init();
      curl_setopt_array($ch, $defaults);
      curl_exec($ch);
      curl_close($ch);
    }
  }
?>
