<?php
	require_once 'sql.php';
	$codigo=$_POST['codigo'];

  $res = array();

	$sql="call consultar_motel($codigo);";
	$mysqli=crearConexion();

  if($resultado = $mysqli->query($sql)){
    $res["datos"] = $resultado->fetch_array(MYSQLI_ASSOC);
    $res["success"] = true;
  }else {
    $res["success"] = false;
  }
  $resultado->free();

	$sql2="SELECT codigo,descripcion,valor,disponibles
		FROM freyjapp.habitaciones_motel
		WHERE motel=$codigo and activo=1
		order by valor;";
  $mysqli->close();
  $mysqli=crearConexion();
  $res["habitaciones"] = array();
  if($resultado = $mysqli->query($sql2)){
    while($row = $resultado->fetch_array(MYSQLI_ASSOC)){
      array_push($res['habitaciones'], $row);
    }
  }else{
    $res["success"] = false;
    $res['error'] = $mysqli->error;
  }
  $resultado->free();
  $mysqli->close();
	header("Content-type: application/json");
	echo json_encode($res);

?>
