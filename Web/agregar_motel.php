<html>
  <head>
    <title>Agregar Motel</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  </head>
  <body>
    <form action="guardar_motel.php" method="post">
      <div class="container" align="center">
      <h2>Agregar Motel</h2>
    <div class="form-group">
      <label for="nombre">Nombre:</label>
      <input type="text" class="form-control" name="nombre" id="nombre">
    </div>
    <div class="form-group">
      <label for="direccion">Direcci&oacute;n:</label>
      <input type="text" class="form-control" name="direccion" id="direccion">
    </div>
    <div class="form-group">
    <div class="selector-dpto">
      <label for="dpto">Departamento</label>
        <select class="form-control" name="dpto" id="dpto"></select>
        <script type="text/javascript">
            $(document).ready(function() {
                $.ajax({
                        type: "POST",
                        url: "listar_dptos.php",
                        success: function(response)
                        {
                            $('.selector-dpto select').html(response).fadeIn();
                        }
                });

            });
        </script>
    </div></div>
    <div class="form-group">
    <div class="selector-ciudad">
            <label for="ciudad">Ciudad</label>
            <select name="ciudad" class="form-control" id="ciudad"></select>
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".selector-dpto select").change(function() {
                        var form_data = {
                                is_ajax: 1,
                                dpto: +$(".selector-dpto select").val()
                        };
                        $.ajax({
                                type: "POST",
                                url: "listar_ciudades.php",
                                data: form_data,
                                success: function(response)
                                {
                                    $('.selector-ciudad select').html(response).fadeIn();
                                }
                        });
                    });

                });
            </script>
        </div></div>
    <div class="form-group">
      <div class="selector-sector">
            <label for="sector">Sector</label>
            <select name="sector" class="form-control" id="sector"></select>
            <script type="text/javascript">
                $(document).ready(function() {
                    $(".selector-ciudad select").change(function() {
                        var form_data = {
                                is_ajax: 1,
                                ciudad: +$(".selector-ciudad select").val()
                        };
                        $.ajax({
                                type: "POST",
                                url: "listar_sectores.php",
                                data: form_data,
                                success: function(response)
                                {
                                    $('.selector-sector select').html(response).fadeIn();
                                }
                        });
                    });

                });
            </script>
        </div></div>
        <div class="form-group">
          <label for="telefono">Tel&eacute;fono:</label>
          <input type="tel" class="form-control" name="telefono" id="telefono">
        </div>
        <div class="form-group">
          <label for="email">E-mail:</label>
          <input type="email" class="form-control" name="email" id="email">
        </div>
        <div class="form-group">
          <label for="minimo">Valor M&iacute;nimo:</label>
          <input type="number" class="form-control" name="minimo" id="minimo">
        </div>
        <div class="form-group">
          <label for="maximo">Valor M&aacute;ximo:</label>
          <input type="number" class="form-control" name="maximo" id="maximo">
        </div>
        <div class="form-group">
          <label for="horas">Horas:</label>
          <input type="number" class="form-control" name="horas" id="horas">
        </div>
        <div class="form-group">
          <label for="latitud">Latitud:</label>
          <input type="number" class="form-control" name="latitud" id="latitud">
        </div>
        <div class="form-group">
          <label for="logintud">Longitud:</label>
          <input type="number" class="form-control" name="longitud" id="longitud">
        </div>
        <div class="form-group">
          <label for="url">Url:</label>
          <input type="url" class="form-control" name="url" id="url">
        </div>
        <input type="submit" class="btn btn-lg btn-default" value="Guardar"/>
        <input type="button" class="btn btn-lg btn-default" onclick="location.href='index.html';" value="Regresar" />
        </div>
    </form>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js" ></script>
  </body>
</html>
