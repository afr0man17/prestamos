<?php
  include_once ('sql.php');
  $mysqli=crearConexion();
  $sql = "select * from moteles where codigo=".$_REQUEST["motel"];
  if($resultado=$mysqli->query($sql)){
    if($resultado->num_rows>0){
      $row = $resultado->fetch_assoc();
      echo ("
      <div class=\"form-group\">
        <label for=\"nombre\">Nombre:</label>
        <input type=\"text\" class=\"form-control\" name=\"nombre\" id=\"nombre\" value=\"".$row['nombre']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"direccion\">Direcci&oacute;n:</label>
        <input type=\"text\" class=\"form-control\" name=\"direccion\" id=\"direccion\" value=\"".$row['direccion']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"telefono\">Tel&eacute;fono:</label>
        <input type=\"tel\" class=\"form-control\" name=\"telefono\" id=\"telefono\" value=\"".$row['telefono']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"email\">E-mail:</label>
        <input type=\"email\" class=\"form-control\" name=\"email\" id=\"email\" value=\"".$row['email']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"minimo\">Valor M&iacute;nimo:</label>
        <input type=\"number\" class=\"form-control\" name=\"minimo\" id=\"minimo\" value=\"".$row['valor_minimo']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"maximo\">Valor M&aacute;ximo:</label>
        <input type=\"number\" class=\"form-control\" name=\"maximo\" id=\"maximo\" value=\"".$row['valor_maximo']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"horas\">Horas:</label>
        <input type=\"number\" class=\"form-control\" name=\"horas\" id=\"horas\" value=\"".$row['horas_ocasional']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"latitud\">Latitud:</label>
        <input type=\"number\" class=\"form-control\" name=\"latitud\" id=\"latitud\" value=\"".$row['latitud']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"logintud\">Longitud:</label>
        <input type=\"number\" class=\"form-control\" name=\"longitud\" id=\"longitud\" value=\"".$row['longitud']."\">
      </div>
      <div class=\"form-group\">
        <label for=\"url\">Url:</label>
        <input type=\"url\" class=\"form-control\" name=\"url\" id=\"url\" value=\"".$row['url']."\">
      </div>
      ");
    }else{
      echo("
      <div class=\"alert alert-danger\">
      <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
        <strong>Error</strong> No se encontr&oacute; informaci&oacute;n. ".$_REQUEST["motel"]."
      </div>
      ");
    }
  }else{
    echo("
    <div class=\"alert alert-danger\">
    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
      <strong>Error</strong> Se gener&oacute; un problema. ".$_REQUEST["motel"]."
    </div>
    ");
  }
  $resultado->free();
  $mysqli->close();
?>
