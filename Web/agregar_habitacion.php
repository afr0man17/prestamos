<html>
  <head>
    <title>Agregar Habitacion</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
    <script src="js/fileinput.min.js" type="text/javascript"></script>
    <link href="css/fileinput.css" media="all" rel="stylesheet" type="text/css" />
    <script>
      function ver(image){
      document.getElementById('image').innerHTML = "<img src='"+image+"'>"
      }
    </script>
    <script type="text/javascript">
    $(document).ready(function(){
      $("#habitacion").submit(function () {
        var comodidades="";
        $("#habitacion").find('input:checkbox').each(function() {
          var elemento= this;
          if(elemento.checked){
            //alert("elemento.name="+ elemento.name + ", elemento.value=" + elemento.value);
            comodidades+=elemento.value+",";
          }
        });
        if(comodidades.value=""){
        //  alert("debe seleccionar al menos una comodidad");
          return false;
        }else{
          //alert(comodidades);
          $("#comodidades").val(comodidades);
          return true;
        }
      });
    });
    // $(document).ready(function(){
    </script>
  </head>
  <body>
    <form  id="habitacion" action="guardar_habitacion.php" method="post" enctype="multipart/form-data">
      <div class="container" align="center">
      <h2>Agregar Habitaci&oacute;n</h2>
      <div class="form-group">
        <div class="selector-motel">
          <label for="motel">Motel:</label>
          <select name="motel" class="form-control" id="motel"></select>
          <script type="text/javascript">
              $(document).ready(function() {
                  $.ajax({
                          type: "POST",
                          url: "listar_moteles.php",
                          success: function(response)
                          {
                              $('.selector-motel select').html(response).fadeIn();
                          }
                  });

              });
          </script>
      </div></div>
      <div class="form-group">
        <label for="nombre">Nombre Habitaci&oacute;n:</label>
        <input type="text" class="form-control" name="nombre" id="nombre">
      </div>
      <div class="form-group">
        <label for="valor">Valor:</label>
        <input type="number" class="form-control" name="valor" id="valor">
      </div>
      <div class="form-group">
        <label for="persona">Persona Adicional:</label>
        <input type="number" class="form-control" name="persona" id="persona">
      </div>
      <div class="form-group">
        <label for="hora">Hora Adicional:</label>
        <input type="number" class="form-control" name="hora" id="hora">
      </div>
      <div class="form-group">
          <label class="control-label" for="foto1">Imagen 1:</label>
          <input name="foto1" id="foto1" class="file" type="file"  data-show-preview="false">
      </div>
      <div class="form-group">
          <label class="control-label" for="foto2">Imagen 2:</label>
          <input name="foto2" id="foto2" class="file" type="file"   data-show-preview="false">
      </div>
      <div class="form-group">
          <label class="control-label" for="foto3">Imagen 3:</label>
          <input name="foto3" id="foto3" class="file" type="file"   data-show-preview="false">
      </div>
      <div class="form-group">
          <label class="control-label" for="foto4">Imagen 4:</label>
          <input name="foto4" id="foto4" class="file" type="file"  onChange="ver(form.file.value)" data-show-preview="false">
      </div>
      <div class="form-group" align="left">
          <label class="control-label">Comodidades</label>
          <?php
            include_once 'sql.php';
            $sql="select codigo,descripcion from comodidades order by descripcion;";
            $mysqli=crearConexion();
            if($resultado=$mysqli->query($sql)){
              while ($row = $resultado->fetch_assoc()) {
                echo '<div class="checkbox"><label><input type="checkbox" name="'.$row['descripcion'].'" value="'.$row['codigo'].'">'.$row['descripcion'].'</label>';
                echo '</div>';
              }
              $resultado->free();
              $mysqli->close();
            }
          ?>
      </div>
      <input type="hidden" id="comodidades" name="comodidades">
      <input type="submit" class="btn btn-lg btn-default" value="Guardar">
      <input type="button" class="btn btn-lg btn-default" onclick="location.href='index.html';" value="Regresar"/>
    </div>
  </form>
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
  <!-- Include all compiled plugins (below), or include individual files as needed -->
  <script src="js/bootstrap.min.js" ></script>
</body>
  </html>
