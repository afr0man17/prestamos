<html>
<head>
  <title>Modificar Motel</title>
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
  </head>
  <body>
    <div class="container" align="center">
    <div class="form-group">
      <input type="button" class="btn btn-lg btn-default" onclick="location.href='index.html';" value="Regresar" />

<?php
  include_once ('sql.php');
  $motel = $_POST["motel"];
  $nombre=$_POST['nombre'];
  $direccion = $_POST['direccion'];
  $telefono=$_POST['telefono'];
  $email=$_POST['email'];
  $minimo=$_POST['minimo'];
  $maximo=$_POST['maximo'];
  $horas=$_POST['horas'];
  $latitud=$_POST['latitud'];
  $longitud=$_POST['longitud'];
  $url=$_POST['url'];
  $sql="update moteles set ".
    "nombre='$nombre',direccion='$direccion',telefono='$telefono',email='$email',valor_minimo=$minimo,valor_maximo=".
    "$maximo,horas_ocasional=$horas,latitud=$latitud,longitud=$longitud,url='$url' where codigo=$motel;";
    $mysqli=crearConexion();
  if($resultado=$mysqli->query($sql)){
    echo("
    <div class=\"alert alert-success\">
    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
      <strong>&Eacute;xito</strong> El motel ha sido actualizado correctamente.
    </div>
    ");
 	}else{
    echo("
    <div class=\"alert alert-danger\">
    <a href=\"#\" class=\"close\" data-dismiss=\"alert\" aria-label=\"close\">&times;</a>
      <strong>Error</strong> No se pudo actualizar el motel.
    </div>
    ");
 	}
  $resultado->free();
  $mysqli->close();
?>
</div></div>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="js/bootstrap.min.js" ></script>
</body>
</html>
