<?php
  function crearConexion(){
    $mysqli =new mysqli(
      getenv('OPENSHIFT_MYSQL_DB_HOST'),
      getenv('OPENSHIFT_MYSQL_DB_USERNAME'),
      getenv('OPENSHIFT_MYSQL_DB_PASSWORD'),
      getEnv("OPENSHIFT_APP_NAME"));
  	      /* verificar la conexión */
    if ($mysqli->connect_errno) {
      printf("Conexión fallida: %s\n", $mysqli->connect_error);
      exit();
    }else{
      $mysqli->query("SET NAMES 'utf8'");
      return $mysqli;
    }
  }
?>
